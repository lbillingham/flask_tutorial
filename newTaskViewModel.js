  function TasksViewModel() {

        // ... same contents and before

        self.add = function(task)
        {
            self.ajax(self.tasksURI, 'POST', task).done(function(data) {
                self.tasks.push({
                    uri: ko.observable(data.task.uri),
                    title: ko.observable(data.task.title),
                    description: ko.observable(data.task.description),
                    done: ko.observable(data.task.done)
                });
            });
        }
    }
    function AddTaskViewModel() {
        var self = this;
        self.title = ko.observable();
        self.description = ko.observable();

        self.addTask = function() {
            $('#add').modal('hide');
            tasksViewModel.add({
                title: self.title(),
                description: self.description()
            });
            self.title("");
            self.description("");
        }
    }

    var tasksViewModel = new TasksViewModel();
    var addTaskViewModel = new AddTaskViewModel();
    ko.applyBindings(tasksViewModel, $('#main')[0]);
    ko.applyBindings(addTaskViewModel, $('#add')[0]);

