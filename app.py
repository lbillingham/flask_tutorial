# -*- coding: utf-8 -*-
"""
Created on Sat Jul 18 18:09:26 2015

@author: laurence
"""
from __future__ import division, print_function

#from flask import fl.Flask, fl.jsonify, fl.abort,  fl.make_response,  fl.request
import flask as fl 

app = fl.Flask(__name__)
    
# quasi database    
tasks = [
            {
             u'id': 1, 
             u'title': u'Buy groceries',
             u'description': u'milk, eggs, pizza, fruit, tylenol',
             u'done': False
             },
            {
             u'id': 2, 
             u'title': u'Flask tutorial',
             u'description': u'need to find a good tutorial',
             u'done': False
             }
        ]
        
# request type handellers
@app.route('/todo/api/v0.1/tasks/', methods=['GET'])
def get_tasks():
    ret = fl.jsonify({u'tasks': [make_public_task(task) for task in tasks]})
    return ret
    
@app.route('/todo/api/v0.1/tasks/<int:task_id>', methods=['GET'])    
def get_task(task_id):
    task = get_task_from_id(task_id)
    return fl.jsonify({u'task': make_public_task(task)}) 
    
@app.route('/todo/api/v0.1/tasks/', methods=['POST'])
def create_task():
    if not  fl.request.json or not u'title' in  fl.request.json:
        fl.abort(400)
    task = {
             u'id': tasks[-1][u'id'] + 1, 
             u'title':  fl.request.json[u'title'],
             u'description':  fl.request.json.get(u'description'),
             u'done':False
            }   
    tasks.append(task)        
    return fl.jsonify({u'task': make_public_task(task)}), 201   

 
@app.route('/todo/api/v0.1/tasks/<int:task_id>', methods=['PUT'])    
def update_task(task_id):    
    task = get_task_from_id(task_id)
    verify_update(task)
    fields = [u'description', u'title', u'done']
    for field in fields:
        task[field] =  fl.request.json.get(field, task[field])
    return fl.jsonify({u'task': make_public_task(task)})    
    
@app.route('/todo/api/v0.1/tasks/<int:task_id>', methods=['DELETE'])    
def delete_task(task_id):    
    task = get_task_from_id(task_id)
    tasks.remove(task)
    return fl.jsonify({u'result': True})       
    
    
# utilities
def make_public_task(task):
    new_task = {}
    for field in task:
        if u'id' == field:
            new_task[u'uri'] = fl.url_for(u'get_task', 
                                          task_id=task[u'id'], 
                                          _external=True)
        else:
            new_task[field] = task[field]
    return new_task       
    
def get_task_from_id(task_id):
    task = [task for task in tasks if task[u'id'] == task_id]
    verify_task_len(task)
    return task[0]
    
## verification    
def verify_task_len(task): 
    if 0 == len(task):
       fl.abort(404)
    
def verify_update(task):
   if not  fl.request.json:
       fl.abort(400)
   fields = [u'description', u'title']
   for field in fields:
       if field in  fl.request.json and type( fl.request.json[field]) is not unicode:
           fl.abort(400)
   if u'done' in  fl.request.json and type( fl.request.json[u'done']) is not bool:
       fl.abort(400)        
    
@app.errorhandler(404)
def not_found(error):
    return fl.make_response(fl.jsonify({u'error': u'Not found'}), 404) 
    
    
if '__main__' == __name__:
    app.run(debug=True)        